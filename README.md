# multi-hosts-swarm

The instruction provides setting up multi-hosts Fabric network based docker swarm tool.

Reference: 
https://kctheservant.medium.com/multi-host-deployment-for-first-network-hyperledger-fabric-v2-273b794ff3d

https://github.com/kctam/4host-swarm.git

# Step 1: Bring up two hosts/VMs in Azure. Ensure the following ports added on all the hosts as inbound port rules:
- TCP port 2377 for cluster management communications
- TCP and UDP port 7946 for communication among nodes
- UDP port 4789 for overlay network traffic

# Step 2: Create an overlay with Docker Swarm

On Host 1:
```bash
docker swarm init --advertise-addr <host-1 ip address>
docker swarm join-token manager
```

Note the output of docker swarm join-token manager as it is being used immediately in next step.

On Host 2:
{output from join-token manager} --advertise-addr {host n ip}

On Host 1, create fabric-network overlay network, which is being used for our network components (see each hostn.yaml in the repository)

```bash
docker network create --attachable --driver overlay fabric-network
```

Now we can check each host. All are sharing the same overlay (note the same network ID in all hosts):
```bash
docker network ls
```

You will see the oveerlay entry from the output like:
```bash
K76xujwms   fabric-network  overlay     swam
```

# Step 3: Clone the repository on all hosts
```bash
git clone https://gitlab.com/yan-he/multi-hosts-swarm
cd multi-hosts-swarm
```

# Step 4: Bring up each host
On Host 1
```bash
./host1up.sh
```

On Host 2
```bash
./host2up.sh
```

Check the containers running on each host:
```bash
docker ps -a
```

# Step 5: Bring up mychannel and join all peers to mychannel

On Host 1
```bash
./mychannelup.sh
```

Confirm if mychannelup.sh run successfully:

On Host 1
```bash
docker exec peer0.org1.example.com peer channel getinfo -c mychannel
docker exec peer0.org2.example.com peer channel getinfo -c mychannel
```

On Host 2
```bash
docker exec peer1.org1.example.com peer channel getinfo -c mychannel
docker exec peer1.org2.example.com peer channel getinfo -c mychannel
```

# Step 6: Deploy chaincode (take fabcar as test chaincode)

6.1 Package chaincode
```bash
cd ~/fabric-samples/chaincode/fabcar$
peer lifecycle chaincode package fabcar.tar.gz --path javascript --lang node --label fabcar_1
```

6.2 Install chaincode

On Host1, run following install commands for all peers.

Install for peer0.org1 on Host 1
```bash
docker exec -e CORE_PEER_ADDRESS=peer0.org1.example.com:7051 -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt -it cli bash
cd /opt/gopath/src/github.com/chaincode/fabcar 
peer lifecycle chaincode install fabcar.tar.gz
```

Install for peer1.org1 on Host 2
```bash
docker exec -e CORE_PEER_ADDRESS=peer1.org1.example.com:8051 -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer1.org1.example.com/tls/ca.crt -it cli bash
cd /opt/gopath/src/github.com/chaincode/fabcar 
peer lifecycle chaincode install fabcar.tar.gz
```

Install for peer0.org2 on Host 1
```bash
docker exec -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp -e CORE_PEER_ADDRESS=peer0.org2.example.com:9051 -e CORE_PEER_LOCALMSPID="Org2MSP" -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -it cli bash
cd /opt/gopath/src/github.com/chaincode/fabcar 
peer lifecycle chaincode install fabcar.tar.gz
```

Install for peer1.org2 oh Host 2
```bash
docker exec -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp -e CORE_PEER_ADDRESS=peer1.org2.example.com:10051 -e CORE_PEER_LOCALMSPID="Org2MSP" -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer1.org2.example.com/tls/ca.crt -it cli bash
cd /opt/gopath/src/github.com/chaincode/fabcar 
peer lifecycle chaincode install fabcar.tar.gz
```

On each host, verify the chaincode got installed:
```bash
docker images dev*
```
Note the the installed chaincode id from the output.

6.3 Approve chaincode for all the organizations

for org1
```bash
docker exec cli peer lifecycle chaincode approveformyorg --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem --channelID mychannel --name fabcar --version 1 --sequence 1 --waitForEvent --package-id fabcar_1:{chaincode_id}
```

for org2
```bash
docker exec -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp -e CORE_PEER_ADDRESS=peer0.org2.example.com:9051 -e CORE_PEER_LOCALMSPID="Org2MSP" -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt cli peer lifecycle chaincode approveformyorg --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem --channelID mychannel --name fabcar --version 1 --sequence 1 --waitForEvent --package-id fabcar_1:{chaincode_id}
```

Check approval status:
```bash
docker exec cli peer lifecycle chaincode checkcommitreadiness --channelID mychannel --name fabcar --version 1 --sequence 1
```

6.4 Commit chaincode

```bash
docker exec cli peer lifecycle chaincode commit -o orderer.example.com:7050 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem --peerAddresses peer0.org1.example.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses peer0.org2.example.com:9051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt --channelID mychannel --name fabcar --version 1 --sequence 1
```

Check commit status:
```bash
docker exec cli peer lifecycle chaincode querycommitted --channelID mychannel --name fabcar
```

# Step 7 : Test chaincode

Invoke initLedger:
```bash
docker exec cli peer chaincode invoke -o orderer.example.com:7050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n fabcar --peerAddresses peer0.org1.example.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses peer0.org2.example.com:9051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '{"Args":["initLedger"]}'
```

Query (queryCar) on Host 1 

For peer0.org1:
```bash
docker exec cli peer chaincode query -n fabcar -C mychannel -c '{"Args":["queryCar","CAR0"]}'
```

For peer1.org1:
```bash
docker exec -e CORE_PEER_ADDRESS=peer1.org1.example.com:8051 -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer1.org1.example.com/tls/ca.crt cli peer chaincode query -n fabcar -C mychannel -c '{"Args":["queryCar","CAR0"]}'
```

For peer0.org2:
```bash
docker exec -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp -e CORE_PEER_ADDRESS=peer0.org2.example.com:9051 -e CORE_PEER_LOCALMSPID="Org2MSP" -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt cli peer chaincode query -n fabcar -C mychannel -c '{"Args":["queryCar","CAR0"]}'
```

For peer1.org2:
```bash
docker exec -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp -e CORE_PEER_ADDRESS=peer1.org2.example.com:10051 -e CORE_PEER_LOCALMSPID="Org2MSP" -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer1.org2.example.com/tls/ca.crt cli peer chaincode query -n fabcar -C mychannel -c '{"Args":["queryCar","CAR0"]}'
```

Verify the results for same query be same across all peers and hosts.